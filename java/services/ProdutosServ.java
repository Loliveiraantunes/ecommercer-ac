/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.google.gson.Gson;
import dao.HibernateUtil;
import dao.imp.CompraDaoImp;
import dao.imp.ProdutoDaoImp;
import dao.interfaceDao.ProdutoDao;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Categoria;
import model.Compra;
import model.Produto;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
@WebServlet(name = "ProdutosServ", urlPatterns = {"/produto"})
@MultipartConfig
public class ProdutosServ extends HttpServlet {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher rd;
    private Produto produto;
    private final ProdutoDao dao;
    private final Session session;
    private Categoria categoria;

    public ProdutosServ() throws ServletException, IOException {
        dao = new ProdutoDaoImp();
        session = HibernateUtil.openSession();
    }

    protected void processRequest()
            throws ServletException, IOException {
        String cmd = request.getParameter("c");
        String paramiter = request.getParameter("p");
        switch (cmd) {
            case "cadastrar":
                salvar();
                break;
            case "jsonListar":
                Jsonlistar();
                break;

            case "shop":
                listar();
                break;

            case "single":
                listarPorId(paramiter);
                break;

            case "buscar":
                buscar(paramiter);
                break;

            case "editar":
                editar(paramiter);
                break;

            case "deletar":
                deletar(paramiter);
                break;
        }
    }

    public void salvar() throws ServletException, IOException {
        String id = request.getParameter("id");
        String nome = request.getParameter("produto");
        String categoriaget = request.getParameter("categoria");
        String preco = request.getParameter("preco").substring(3);
        String detalhe = request.getParameter("detalhe");
        String descricao = request.getParameter("descricao");
        String tag = request.getParameter("tag");
        Part filePrincipal = request.getPart("principal");
        Part file2 = request.getPart("img2");
        Part file3 = request.getPart("img3");
        try {
            produto = new Produto();
            categoria = new Categoria();
            
            if(id != null){
                produto.setId(Long.parseLong(id));
            }
            
            categoria.setId(Long.parseLong(categoriaget));
            produto.setNome(nome);
            produto.setCategoria(categoria);
            produto.setPreco(Double.parseDouble(preco));
            produto.setDescricao(descricao);
            produto.setDescricaodetalhe(detalhe);

            produto.setTag(tag);
            session.clear();

            produto.setImg(convertInputStream(filePrincipal));
            produto.setImg2(convertInputStream(file2));
            produto.setImg3(convertInputStream(file3));

            dao.SalvarOuAlterar(produto, session);
            request.setAttribute("produto", null);
            request.getRequestDispatcher("admin/dashboard.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao salvar " + ex.getMessage());
        }
    }

    public void editar(String id) throws ServletException, IOException {
        Produto produto = dao.getById("Produto", Long.parseLong(id), session);
        request.setAttribute("produto", produto);
        request.getRequestDispatcher("admin/dashboard.jsp").forward(request, response);
    }

    public void deletar(String id) throws ServletException, IOException {
        String produtoId = id;
        Produto produto = dao.getById("Produto", Long.parseLong(id), session);
        for (Compra compra : produto.getCompra()) {
            new CompraDaoImp().excluir(compra, session);
        }
        try {
            session.clear();
            dao.excluir(produto, session);
            request.setAttribute("produtoDeletar", 1);
            request.getRequestDispatcher("admin/dashboard.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao DELETAR: " + ex.getMessage());
        }

    }

    public void listar() throws ServletException, IOException {
        try {
            List<Produto> produtos = dao.getAll("Produto", session);
            request.setAttribute("produtos", produtos);
            request.getRequestDispatcher("shop/shop-grid.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao Listar: " + ex.getMessage());
        }
    }

    public void buscar(String p) throws ServletException, IOException {
        try {
            List<Produto> produtos = null;

            String nome = request.getParameter("nome");
            if (nome != null) {
                produtos = dao.getByNome(nome, session);
            }

            if (p != null) {
                produtos = dao.getByCategoriaORTag(p, session);
            }

            request.setAttribute("produtos", produtos);
            request.getRequestDispatcher("shop/shop-grid.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao Listar: " + ex.getMessage());
        }
    }

    public void listarPorId(String id) throws ServletException, IOException {
        try {
            Produto produto = dao.getById("Produto", Long.parseLong(id), session);
            List<Produto> produtos = dao.getAll("Produto", session);
            request.setAttribute("produto", produto);
            request.setAttribute("produtos", produtos);
            request.getRequestDispatcher("shop/single-product.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao Listar: " + ex.getMessage());
        }
    }

    public void Jsonlistar() throws IOException {
        try {
            List<Produto> produtos = dao.getAll("Produto", HibernateUtil.openSession());
            List<Produto> produtosJson = new ArrayList<>();
            for (Produto pro : produtos) {
                pro.setCategoria(null);
                pro.setCompra(null);
                produtosJson.add(pro);
            }
            String json = new Gson().toJson(produtosJson);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        } catch (HibernateException ex) {
            System.out.println("Erro ao listar: " + ex.getMessage());
        }
    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    private String convertInputStream(Part part) throws IOException {
        if (part != null) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            InputStream inputStream = null;
            if (part != null) {
                long fileSz = part.getSize();
                String fileContent = part.getContentType();
                inputStream = part.getInputStream();

            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[16384];
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            String encodedString = Base64.getEncoder().encodeToString(buffer.toByteArray());
            return encodedString;
        }
        return null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

}
