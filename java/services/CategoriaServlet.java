/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.google.gson.Gson;
import dao.HibernateUtil;
import dao.imp.CategoriaDaoImp;
import dao.interfaceDao.CategoriaDao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Categoria;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
@WebServlet(name = "CategoriaServlet", urlPatterns = {"/categoria"})
public class CategoriaServlet extends HttpServlet {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher rd;
    private CategoriaDao dao;
    private final Session session;

    public CategoriaServlet() {
        dao = new CategoriaDaoImp();
        session = HibernateUtil.openSession();
    }

    protected void processRequest()
            throws ServletException, IOException {
        String var = request.getParameter("c");
        switch (var) {
            case "cadastrar":
                salvar();
                listar();
                break;
            case "listar":
                listar();
                break;
            case "excluir":
                excluir();
                listar();
                break;
            case "jsonListar":
                Jsonlistar();
                break;

        }
       
    }

    public void salvar() throws ServletException, IOException {
        String cat = request.getParameter("categoria");
        try {
            Categoria categoria = new Categoria();
            categoria.setNome(cat);
            dao.SalvarOuAlterar(categoria, session);
            request.setAttribute("categoriaCadastrada", 1);
          request.getRequestDispatcher("admin/dashboard.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao salvar " + ex.getMessage());
        }
    }

    public void excluir() throws ServletException, IOException {
        String cat = request.getParameter("id");
        Categoria categoria = new Categoria();
        categoria.setId(Long.parseLong(cat));
        try {
            session.clear();
            dao.excluir(categoria, session);
            request.setAttribute("categoriaCadastrada", 1);
           request.getRequestDispatcher("admin/dashboard.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao salvar " + ex.getMessage());
        }
    }

    public void listar() throws ServletException, IOException {
        try {
            List<Categoria> categorias = dao.getAll("Categoria", session);
            request.setAttribute("categorias", categorias);
            request.getRequestDispatcher("admin/dashboard.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao salvar " + ex.getMessage());
        }
    }

    public void Jsonlistar() throws IOException {
        try {
            List<Categoria> categorias = dao.getAll("Categoria", HibernateUtil.openSession());
            List<Categoria> categoriasJson = new ArrayList<>() ;
            for(Categoria catjs: categorias){
                catjs.setProdutos(null);
                categoriasJson.add(catjs);
            }
            String json = new Gson().toJson(categoriasJson);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        } catch (HibernateException ex) {
            System.out.println("Erro ao listar: " + ex.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

}
