/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.HibernateUtil;
import dao.imp.AdminDaoImp;
import dao.interfaceDao.AdminDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Admin;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
@WebServlet(name = "AdminServlet", urlPatterns = {"/adm"})
public class AdminServlet extends HttpServlet {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher rd;
    private AdminDao adminDao;
    private Session session;

    public AdminServlet() {
        adminDao = new AdminDaoImp();
        session = HibernateUtil.openSession();
    }

    protected void processRequest()
            throws ServletException, IOException {
        String var = request.getParameter("c");
        switch (var) {
            case "login":
                login();
                break;
            case "deslogar":
           request.getSession().setAttribute("logado", 0);
           rd = request.getRequestDispatcher("admin/dashboard.jsp");
                break;
        }
        rd.forward(request, response);
    }

    public void login() throws IOException, ServletException {
        String login = request.getParameter("login");
        String senha = request.getParameter("password");
        try {
            Admin admin = adminDao.logar(login, senha, session);

            if (admin != null) {
                request.getSession().setAttribute("logado", 1);
                request.getSession().setAttribute("login", null);
            } else {
                request.getSession().setAttribute("logado", 0);
                request.setAttribute("login", "Email ou Senha incorreta");
                response.setHeader("admin/dashboard.jsp", "");
            }
            rd = request.getRequestDispatcher("admin/dashboard.jsp");
        } catch (HibernateException ex) {
            System.out.println("Erro ao pesquisar " + ex.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

}
