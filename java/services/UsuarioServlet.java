/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.HibernateUtil;
import dao.imp.EnderecoDaoImp;
import dao.imp.UsuarioDaoImp;
import dao.interfaceDao.EnderecoDao;
import dao.interfaceDao.UsuarioDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Endereco;
import model.Usuario;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
@WebServlet(name = "UsuarioServlet", urlPatterns = {"/usuario"})
public class UsuarioServlet extends HttpServlet {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher rd;
    private UsuarioDao dao;
    private Session session;
    private Usuario usuario;

    public UsuarioServlet() {
        dao = new UsuarioDaoImp();
        session = HibernateUtil.openSession();
    }

    protected void processRequest()
            throws ServletException, IOException {
        String var = request.getParameter("c");
        switch (var) {
            case "logar":
                logar();
                break;
            case "cadastrar":
                salvar();
                break;
        }

    }

    public void salvar() throws ServletException, IOException {

        String nome = request.getParameter("nome");
        String sobrenome = request.getParameter("sobrenome");
        String email = request.getParameter("email");
        String cpf = request.getParameter("cpf");
        String idade = request.getParameter("idade");
        String celular = request.getParameter("celular");
        String telefone = request.getParameter("telefone");
        String telComercial = request.getParameter("telComercial");
        String password = request.getParameter("senha");

        String logradouro = request.getParameter("logradouro");
        String bairro = request.getParameter("bairro");
        String numero = request.getParameter("numero");
        String cep = request.getParameter("cep");
        String cidade = request.getParameter("cidade");
        String estado = request.getParameter("estado");
        String complemento = request.getParameter("complemento");

        usuario = new Usuario();

        Endereco endereco = new Endereco();

        endereco.setBairro(bairro);
        endereco.setCep(Integer.parseInt(cep.replaceAll("-", "")));
        endereco.setEstado(estado);
        endereco.setLogradouro(logradouro);
        endereco.setComplemento(complemento);
        endereco.setNumero(Integer.parseInt(numero));
        endereco.setCidade(cidade);

        usuario.setNome(nome);

        usuario.setSobrenome(sobrenome);

        usuario.setEmail(email);

        usuario.setCpf(cpf);

        usuario.setIdade(Integer.parseInt(idade));
        usuario.setCelular(celular);

        usuario.setTelefone(telefone);

        usuario.setTelcomercial(telComercial);

        usuario.setSenha(password);

        try {
            dao.SalvarOuAlterar(usuario, session);
            usuario = dao.getLogin(usuario.getEmail(), usuario.getSenha(), session);
            endereco.setUsuario(usuario);
            new EnderecoDaoImp().SalvarOuAlterar(endereco, session);

            request.setAttribute("msg", "Salvo com Sucesso");
            request.setAttribute("usuario", usuario);
            request.getRequestDispatcher("shop/usuario.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao salvar: " + ex.getMessage());
        }
    }

    public void logar() throws ServletException, IOException {
        try {

            String email = request.getParameter("email");
            String senha = request.getParameter("senha");
            usuario = dao.getLogin(email, senha, session);

            if (usuario != null) {
                request.getSession().setAttribute("userLogado", usuario.getId());
                 request.getRequestDispatcher("produto?c=shop").forward(request, response);
            } else {
                request.setAttribute("msg", "Usuario ou senha incorreta");
                 request.getRequestDispatcher("/shop/usuario.jsp").forward(request, response);
            }
           
        } catch (HibernateException ex) {
            System.out.println("Erro ao logar: " + ex.getMessage());
            request.getRequestDispatcher("/shop/usuario.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }
}
