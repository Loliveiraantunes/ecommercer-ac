/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.HibernateUtil;
import dao.imp.CompraDaoImp;
import dao.imp.ProdutoDaoImp;
import dao.imp.UsuarioDaoImp;
import dao.interfaceDao.CompraDao;
import dao.interfaceDao.UsuarioDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Compra;
import model.Produto;
import model.Usuario;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
@WebServlet(name = "CheckoutServlet", urlPatterns = {"/checkout"})
public class CheckoutServlet extends HttpServlet {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher rd;
    private CompraDao cDao;
    private final Session session;

    public CheckoutServlet() {
        cDao = new CompraDaoImp();
        this.session = HibernateUtil.openSession();
    }

    protected void processRequest()
            throws ServletException, IOException {
        String var = request.getParameter("c");
        switch (var) {
            case "finalizar":
                finalizar();
                break;
        }
    }

    public void finalizar() throws ServletException, IOException {
        String idUser = request.getParameter("userId");
        String totalProdutos = request.getParameter("totalProdutos");
        String totalCompra = request.getParameter("totalCompra");
        try {
            Usuario usuario = new UsuarioDaoImp().getById("Usuario", Long.parseLong(idUser), session);
            List<Compra> compras = cDao.getCompras(idUser, session);
            request.setAttribute("user", usuario);
            request.setAttribute("compras", compras);
            request.setAttribute("totalProdutos", totalProdutos);
            request.setAttribute("totalCompra", totalCompra);
            request.getRequestDispatcher("/shop/checkout.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao Listar:" + ex.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

}
