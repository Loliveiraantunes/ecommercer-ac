/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.HibernateUtil;
import dao.imp.CompraDaoImp;
import dao.imp.ProdutoDaoImp;
import dao.imp.UsuarioDaoImp;
import dao.interfaceDao.CompraDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Compra;
import model.Produto;
import model.Usuario;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
@WebServlet(name = "CompraServlet", urlPatterns = {"/compra"})
public class CompraServlet extends HttpServlet {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher rd;
    private CompraDao dao;
    private final Session session;

    public CompraServlet() {
        dao = new CompraDaoImp();
        this.session = HibernateUtil.openSession();
    }

    protected void processRequest()
            throws ServletException, IOException {
        String var = request.getParameter("c");
        switch (var) {
            case "cadastrar":
                salvar();
                break;
            case "carrinho":
                carrinho();
                break;
            case "remover":
                remover();
                break;
            case "atualizarCarrinho":
                atualizarCarrinho();
                break;

        }

    }

    public void salvar() throws ServletException, IOException {
        String idProduto = request.getParameter("idProduto");
        String idUser = request.getParameter("idUser");
        int quantidade = 0;
        if (!request.getParameter("quantidade").isEmpty()) {
            quantidade = Integer.parseInt(request.getParameter("quantidade"));
        }

        try {
            Produto produto = new ProdutoDaoImp().getById("Produto", Long.parseLong(idProduto), session);
            Usuario usuario = new UsuarioDaoImp().getById("Usuario", Long.parseLong(idUser), session);
            Compra compra = dao.getCompra(idUser, idProduto, session);
            if (compra == null) {
                compra = new Compra();

                if (quantidade == 0) {
                    quantidade++;
                    compra.setQuantidade(quantidade);
                } else {
                    compra.setQuantidade(quantidade);
                }
                compra.setValorTotal((Double) (quantidade * produto.getPreco()));
                compra.setProduto(produto);
                compra.setUsuario(usuario);
            } else {
                if (quantidade == 0) {
                    compra.setQuantidade(compra.getQuantidade() + 1);
                } else {
                    compra.setQuantidade(quantidade);
                }
                compra.setValorTotal(compra.getQuantidade() * produto.getPreco());
            }

            dao.SalvarOuAlterar(compra, session);
            request.setAttribute("msg", "Salvo com Sucesso");
            request.getRequestDispatcher("compra?c=carrinho&p=" + idUser).forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao salvar " + ex.getMessage());
        }
    }

    public void atualizarCarrinho() throws ServletException, IOException {
        String idUser = request.getParameter("p");
        try {
            List<Compra> compras = dao.getCarrinho(idUser, session);

            for (Compra compra : compras) {
                String idProduto = request.getParameter("idProduto-" + compra.getId());
                int quantidade = Integer.parseInt(request.getParameter("quantidade-" + compra.getId()));

                Produto produto = new ProdutoDaoImp().getById("Produto", Long.parseLong(idProduto), session);
                Usuario usuario = new UsuarioDaoImp().getById("Usuario", Long.parseLong(idUser), session);

                Compra compraEdit = dao.getCompra(idUser, idProduto, session);
                if (compraEdit == null) {
                    compraEdit = new Compra();
                    if (quantidade == 0) {
                        quantidade++;
                        compraEdit.setQuantidade(quantidade);
                    } else {
                        compraEdit.setQuantidade(quantidade);
                    }
                    compraEdit.setValorTotal((Double) (quantidade * produto.getPreco()));
                    compraEdit.setProduto(produto);
                    compraEdit.setUsuario(usuario);
                } else {
                    compraEdit.setQuantidade(quantidade);
                    compraEdit.setValorTotal(compra.getQuantidade() * produto.getPreco());
                }

                dao.SalvarOuAlterar(compraEdit, session);
            }
            carrinho();
        } catch (HibernateException ex) {
            System.out.println("Erro ao Listar: " + ex.getMessage());
        }
    }

    public void carrinho() throws ServletException, IOException {
        String idUser = request.getParameter("p");
        try {
            List<Compra> compras = dao.getCarrinho(idUser, session);

            Double total = 0D;
            Double totalCompra = 0D;
            for (Compra compra : compras) {
                if (compra.getQuantidade() <= 0) {
                    dao.excluir(compra, session);
                } else {
                    total += compra.getProduto().getPreco();
                    totalCompra += compra.getValorTotal();
                }
            }

            compras = dao.getCarrinho(idUser, session);
            
            request.setAttribute("userId", idUser);
            request.setAttribute("compras", compras);
            request.setAttribute("totalProdutos", total);
            request.setAttribute("totalCompra", totalCompra);
            request.getRequestDispatcher("shop/cart.jsp").forward(request, response);
        } catch (HibernateException ex) {
            System.out.println("Erro ao Listar: " + ex.getMessage());
        }

    }

    private void remover() throws ServletException, IOException {
        String id = request.getParameter("r");
        try {
            Compra compras = dao.getById("Compra", Long.parseLong(id), session);
            dao.excluir(compras, session);
            carrinho();
        } catch (HibernateException ex) {
            System.out.println("Erro ao remover: " + ex.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

}
