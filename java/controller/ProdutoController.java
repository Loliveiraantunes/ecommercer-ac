/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateUtil;
import dao.imp.ProdutoDaoImp;
import dao.interfaceDao.ProdutoDao;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import model.Produto;
import org.hibernate.HibernateException;
import org.hibernate.Session;

@Named(value = "ProductC")
public class ProdutoController {

    private Session session;

    private Produto produto;
    private ProdutoDao produtoDao;
    private DataModel<Produto> produtosModel;

    public ProdutoController() {
        produtoDao = new ProdutoDaoImp();
    }

    private void abreSessao() {
        session = HibernateUtil.openSession();
    }

    public void getProducts() {
        abreSessao();
        try {
            List<Produto> produtos = produtoDao.getAll(session);
            produtosModel = new ListDataModel<>(produtos);
        } catch (HibernateException he) {
            System.out.println("404 - Nenhum Registro encontrado");
        } finally {
            session.close();
        }
    }
    
    public void saveProduct(){
         abreSessao();
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            produtoDao.SalvarOuAlterar(produto, session);
            produto = new Produto();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Salvo com sucesso", ""));
        } catch (HibernateException e) {
            System.out.println("erro ao salvar " + e.getMessage());
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar", ""));
        }
    }
}
