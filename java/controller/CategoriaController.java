/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateUtil;
import dao.imp.CategoriaDaoImp;
import dao.interfaceDao.CategoriaDao;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import model.Categoria;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
public class CategoriaController {

    private Session session;
    public Categoria categoria;
    private CategoriaDao categoriaDao;
    public DataModel<Categoria> model;

    public CategoriaController() {
        categoriaDao = new CategoriaDaoImp();
    }
    
    
    
    public void getAll(){
         HibernateUtil.openSession();
           try {
            List<Categoria>  categorias = categoriaDao.getAll("Categoria",session);
             model = new ListDataModel<>(categorias);
        } catch (HibernateException he) {
            System.out.println("404 - Nenhum Registro encontrado");
        } finally {
             HibernateUtil.openSession().close();
        }
    }

    public void save() {
        HibernateUtil.openSession();
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            System.out.print("Teste");
            categoriaDao.SalvarOuAlterar(categoria, session);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Salvo com sucesso", ""));
        } catch (HibernateException he) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar", ""));
        }
        HibernateUtil.openSession().close();
    }
    
    
    
    public Categoria getCategoria(){
        return categoria;
    }

}
