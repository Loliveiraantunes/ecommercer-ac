/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import model.Admin;
import model.Categoria;
import model.Compra;
import model.Endereco;
import model.Produto;
import model.Usuario;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author leofr
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
          Configuration cfg = new Configuration();
            cfg.addAnnotatedClass(Admin.class);
            cfg.addAnnotatedClass(Categoria.class);
            cfg.addAnnotatedClass(Produto.class);
            cfg.addAnnotatedClass(Compra.class);
            cfg.addAnnotatedClass(Usuario.class);
            cfg.addAnnotatedClass(Endereco.class);

            
            cfg.configure("/dao/hibernate.cfg.xml");
            StandardServiceRegistryBuilder build = 
                    new StandardServiceRegistryBuilder().
                            applySettings(cfg.getProperties());
            sessionFactory = cfg.buildSessionFactory(build.build());          

        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static Session openSession() {
        return sessionFactory.openSession();
    }
}
