/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaceDao;

import dao.BaseDAO;
import model.Usuario;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
public interface UsuarioDao extends BaseDAO<Usuario, Long>{
    Usuario getLogin(String email,String senha, Session session) throws HibernateException;
}
