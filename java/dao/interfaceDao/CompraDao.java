/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaceDao;

import dao.BaseDAO;
import java.util.List;
import model.Compra;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
public interface CompraDao extends BaseDAO<Compra, Long>{
    List<Compra> getCarrinho(String userId,Session session) throws HibernateException;
    
      Compra getCompra(String userId,String produtoId,Session session) throws HibernateException;
      
      List<Compra> getCompras(String userId,Session session) throws HibernateException;
}
