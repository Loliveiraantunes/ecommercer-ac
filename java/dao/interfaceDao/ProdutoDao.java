/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaceDao;

import dao.BaseDAO;
import java.util.List;
import model.Produto;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
public interface ProdutoDao extends BaseDAO<Produto, Long> {

    List<Produto> getAll(Session session) throws HibernateException;

    List<Produto> getByNome(String nome, Session session) throws HibernateException;

    List<Produto> getByCategoriaORTag(String nome, Session session) throws HibernateException;

}
