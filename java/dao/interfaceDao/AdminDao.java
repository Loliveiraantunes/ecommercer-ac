/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.interfaceDao;

import dao.BaseDAO;
import java.util.List;
import model.Admin;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
public interface AdminDao extends BaseDAO<Admin,Long>{
   Admin logar(String login,String senha, Session session) throws HibernateException;
}
