/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class BaseDAOImp<T, ID> implements BaseDAO<T, ID> {

    private Transaction transaction;

    @Override
    public void SalvarOuAlterar(T entidade, Session session) throws HibernateException {
        transaction = session.beginTransaction();
        session.saveOrUpdate(entidade);
        transaction.commit();
    }

    @Override
    public void excluir(T entidade, Session session) throws HibernateException {
        transaction = session.beginTransaction();
        session.delete(entidade);
        transaction.commit();
    }

    @Override
    public List<T> getAll(String classe, Session session) throws HibernateException {
        Query consulta = session.createQuery("from " + classe + " c");
        return consulta.list();
    }

    @Override
    public T getById(String classe, ID id, Session session) throws HibernateException {
        Query consulta = session.createQuery("from " + classe + " where id = :id");
        consulta.setParameter("id", id);
        return (T) consulta.uniqueResult();
    }

}
