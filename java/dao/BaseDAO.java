/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author leofr
 * @param <T>
 * @param <ID>
 */
public interface BaseDAO<T, ID> {

    void SalvarOuAlterar(T entidade, Session session) throws HibernateException;

    void excluir(T entidade, Session session) throws HibernateException;

    List<T> getAll(String classe, Session session) throws HibernateException;

    T getById(String classe, ID id, Session session) throws HibernateException;

}
