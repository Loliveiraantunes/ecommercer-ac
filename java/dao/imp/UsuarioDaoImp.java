package dao.imp;

import dao.BaseDAOImp;
import dao.interfaceDao.UsuarioDao;
import model.Usuario;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

public class UsuarioDaoImp extends BaseDAOImp<Usuario, Long> implements UsuarioDao {

    @Override
    public Usuario getLogin(String email, String senha, Session session) throws HibernateException {
        Query consulta = session.createQuery("from Usuario u where email = :email and senha = :senha");
        consulta.setParameter("email", email);
        consulta.setParameter("senha", senha);
        return (Usuario) consulta.uniqueResult();
    }

}
