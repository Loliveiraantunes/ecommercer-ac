/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.imp;

import dao.BaseDAOImp;
import dao.interfaceDao.AdminDao;
import java.util.List;
import model.Admin;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
public class AdminDaoImp extends BaseDAOImp<Admin, Long> implements AdminDao{

    @Override
    public Admin logar(String login, String senha, Session session) throws HibernateException {
    Query consulta = session.createQuery("from Admin a where email = :login and senha = :senha ");
    consulta.setParameter("login",login);
    consulta.setParameter("senha",senha);
     return (Admin)consulta.uniqueResult();
    }
    
    
}
