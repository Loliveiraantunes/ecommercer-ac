/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.imp;

import dao.BaseDAOImp;
import dao.interfaceDao.ProdutoDao;
import java.io.Serializable;
import java.util.List;
import model.Produto;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author leofr
 */
public class ProdutoDaoImp extends BaseDAOImp<Produto, Long> implements ProdutoDao, Serializable {

    @Override
    public List<Produto> getAll(Session session) throws HibernateException {
        Query consulta = session.createQuery("from Produto p");
        return consulta.list();
    }

    @Override
    public List<Produto> getByNome(String nome, Session session) throws HibernateException {
        Query consulta = session.createQuery("from Produto p where p.nome like :nome");
        consulta.setParameter("nome","%"+nome+"%");
        return consulta.list();
    }

    @Override
    public List<Produto> getByCategoriaORTag(String nome, Session session) throws HibernateException {
         Query consulta = session.createQuery("from Produto p where (p.categoria.nome like :nome OR p.tag like :tag) ");
        consulta.setParameter("nome","%"+nome+"%");
         consulta.setParameter("tag","%"+nome+"%");
        return consulta.list();
    }

   
}
