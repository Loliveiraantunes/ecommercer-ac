/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.imp;

import dao.BaseDAOImp;
import dao.interfaceDao.CompraDao;
import java.util.List;
import model.Compra;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

public class CompraDaoImp extends BaseDAOImp<Compra, Long> implements CompraDao {

    @Override
    public List<Compra> getCarrinho(String userId, Session session) {
        Query consulta = session.createQuery("from Compra c where usuario.id = :id");
        consulta.setParameter("id", Long.parseLong(userId));
        return consulta.list();
    }

    @Override
    public Compra getCompra(String userId, String produtoId, Session session) throws HibernateException {
        Query consulta = session.createQuery("from Compra c where c.usuario.id = :idUser and c.produto.id = :idProduto ");
        consulta.setParameter("idUser", Long.parseLong(userId));
        consulta.setParameter("idProduto", Long.parseLong(produtoId));
        return (Compra) consulta.uniqueResult();
    }

    @Override
    public List<Compra> getCompras(String userId, Session session) throws HibernateException {
        Query consulta = session.createQuery("from Compra c where c.usuario.id = :idUser");
        consulta.setParameter("idUser", Long.parseLong(userId));
        return consulta.list();
    }

}
